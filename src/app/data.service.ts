import { Injectable } from '@angular/core';
import {loadMockedData} from './mockedData';

@Injectable()
export class DataService {

  constructor() { }
  getData() {
    return loadMockedData();
  }

}
