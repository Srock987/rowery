import {Component, Input} from '@angular/core';
import {Rower} from './Rower';

@Component({
  selector: 'rower-detail',
  template:  `
    <div class="rowerDetail" *ngIf="rower">
      <div *ngIf="rower.amount!==0" class="row">
        <div class="col-6">
          <img class="img-fluid" src='{{rower.pictureUri}}'/>
        </div>
        <div class="col-6">
          <h4>{{rower.name | uppercase}}</h4>
          <p>OPIS: {{rower.description}}</p>
          <p>ILOSC: {{rower.amount}}</p>
          <p>CENA: {{rower.price | currency:'USD':'symbol'}}</p>
            <button class="orderButton" *ngIf="rower.amount>0" (click)="orderBike()">+</button>
            <button class="disorderButton" *ngIf="orderedAmount>0" (click)="disoderBike()">-</button>
            <p class="orderInfo" *ngIf="orderedAmount>0"> Zamówinych sztuk: {{orderedAmount}}</p>
        </div>
      </div>
      <div class="row" *ngIf="rower.amount===0">
        <div class="col-6">
          <img class="img-fluid" src='{{rower.pictureUri}}'/>
        </div>
        <div class="col-6">
          <h4>{{rower.name | uppercase}}</h4>
          <p>OPIS: {{rower.description}}</p>
          <p>Wybrany produkt nie jest dostępny. </p>
          <p>Ilosć sztuk w magazynie wynosi {{rower.amount}}.</p>
            <button class="disorderButton" *ngIf="orderedAmount>0" (click)="disoderBike()">-</button>
            <p class="orderInfo" *ngIf="orderedAmount>0"> Zamówinych sztuk: {{orderedAmount}}</p>
        </div>
      </div>
    </div>`,
  styles: [`
    .rowerDetail {
      padding: 35px;
      background-color: #EEE;
      opacity: 0.9;
    }
    
    .rowerDetail img{
      justify-content: center
    }

    .orderButton {
      float: left;
      font-size: large;
      padding-left: 20px;
      padding-right: 20px;
      padding-top: 10px;
      padding-bottom: 10px;
      background-color: #14e224;
      opacity: 1;
    }

    .disorderButton {
      float: right;
      font-size: large;
      padding-left: 20px;
      padding-right: 20px;
      padding-top: 10px;
      padding-bottom: 10px;
      background-color: #e2355c;
      margin: 5px;
      opacity: 1;
    }
    
    .orderInfo {
      float: inherit;
      text-align: center
    }
  `],
})

export class RowerDetailComponent {
 @Input() rower: Rower;
 orderedAmount: number = 0;
 orderBike() {
   this.orderedAmount++;
   this.rower.amount--;
 }
 disoderBike() {
   this.orderedAmount--;
   this.rower.amount++;
 }
}

