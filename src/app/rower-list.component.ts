import {Component} from '@angular/core';
import {Rower} from './Rower';
import {DataService} from './data.service';

@Component({
  selector: 'rower-list' ,
  template: `    
      <div class="rowerInstance" *ngFor="let rower of ROWERY">
        <rower-detail [rower]="rower"></rower-detail>
      </div>
      <div class="bottomSummary">
        <p>W magaznie mamy {{sumBikes()}} rowerów.</p>
      </div>
  `,
  styles: [`
    .rowerInstance {
      margin: 20px;
    }
    .bottomSummary {
      padding: 10px;
      background-color: #EEE;
      text-align: center;
    }
  `],
})


export class RowerListComponent {
  ROWERY: Rower[] = this.dataService.getData();
  constructor(private dataService: DataService) {
  }
  sumBikes() {
    let ilosc = 0;
    for (let rower of this.ROWERY) {
      ilosc += rower.amount;
    }
    return ilosc;
  }
}
