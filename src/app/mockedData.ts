import {Rower} from './Rower';

export function loadMockedData() {let ROWERY: Rower[] = [{id: 0,
    name: 'Wigry 1',
    description: 'Super rower',
    amount: 5,
    price: 10,
    pictureUri: 'rower1.jpg'
  }, {id: 1,
  name: 'Wigry 1',
  description: 'Super rower',
  amount: 5,
  price: 10,
  pictureUri: 'rower1.jpg'
  }, {
    id: 2,
    name: 'Góral',
    description: 'Gorski rower',
    amount: 3,
    price: 20,
    pictureUri: 'rower2.jpg'
  }, {
    id: 3,
    name: 'Kolazowka',
    description: 'Szybki rower',
    amount: 2,
    price: 30,
    pictureUri: 'rower3.jpg'
  }, {
    id: 4,
    name: 'Rastabike',
    description: 'Extra rower',
    amount: 0,
    price: 40,
    pictureUri: 'rower4.jpg'
  }, {
    id: 5,
    name: 'SpeedBike',
    description: 'Prawie rower',
    amount: 5,
    price: 50,
    pictureUri: 'rower5.jpg'
  }];
  return ROWERY;
}
