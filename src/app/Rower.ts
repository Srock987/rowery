export class Rower {
  id: number;
  name: string;
  description: string;
  amount: number;
  price: number;
  pictureUri: string;
}
