import { Component } from '@angular/core';


@Component({
  selector: 'my-app',
  template: `
    <div class="page">
    <div class="header">
      <h1>Zamawianie rowerów</h1>
    </div>
    <rower-list></rower-list>
    </div>
  `,
  styles: [`
    .page{
      background-image: url("mountains.jpg");
    }
    .header {
      padding: 10px;
      background-color: cornflowerblue;
      text-align: center;
    }
  `]

})


export class AppComponent  {
}




