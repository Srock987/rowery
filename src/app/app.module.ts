import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import {RowerListComponent} from './rower-list.component';
import {RowerDetailComponent} from './rower-detail.component';
import {DataService} from './data.service';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent,
  RowerListComponent,
  RowerDetailComponent],
  providers:    [DataService],
  bootstrap:    [ AppComponent,
    RowerListComponent,
    RowerDetailComponent ]
})
export class AppModule { }
